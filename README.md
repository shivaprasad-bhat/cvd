# Sukhi Hrudaya
## CVD Care Applicatioon

>Sukhi Hrudaya is a web application developed for a team of academicians and students from Manipal Academy of Higher Education.This portal is designed for a Cardio-vascular Diseases Care Program, for the Management of CVDs among pre-menopausal and peri-menopausal women. The development is funded by the Public Health Foundation of India (PFHI), a statutory body under the Department of Science and Technology. This application contains static pages which are available for everyone and a portal where caregivers has to login to access data, take surveys, collect patient details and other patient management features.

